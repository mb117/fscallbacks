/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs").promises;
const path = require("path");

function readAndManipulateFiles() {
    const lipsumPath = "../lipsum.txt";

    fs.readFile(lipsumPath, { flag: "r", encoding: "utf-8" })
        .then((data) => {
            console.log("Successfully read lipsum.txt");
            data = data.toUpperCase();

            return fs.writeFile("./uppercaseLipsum.txt", data, { encoding: "utf-8", flag: "w" });
        })
        .then(() => {
            console.log("Successfully written uppercase data to uppercaseLipsum.txt");

            return fs.writeFile("./filenames.txt", "uppercaseLipsum.txt", { encoding: "utf-8", flag: "w" });
        })
        .then(() => {
            console.log("Successfully written upercaseLipsum.txt to filenames.txt");

            return fs.readFile("./uppercaseLipsum.txt", { flag: "r", encoding: "utf-8" });
        })
        .then((data) => {
            data = data.toLowerCase()
                .split(".")
                .map((sentence) => {
                    return sentence.trim();
                })
                .filter((sentence) => {
                    return sentence !== "";
                })
                .join("\n");

            console.log("Successfully converted to lowercase data");

            return fs.writeFile("./lowercaseIpsum.txt", data, { encoding: "utf-8", flag: "w" });

        })
        .then(() => {
            console.log("Successfully written lowercase data to lowercaseIpsum.txt");

            return fs.writeFile("./filenames.txt", "\nlowercaseIpsum.txt", { encoding: "utf-8", flag: "a" });
        })
        .then(() => {
            console.log("Successfully written lowercaseIpsum.txt to filenames.txt");

            return Promise.all([fs.readFile("./uppercaseLipsum.txt", { flag: "r", encoding: "utf-8" }),
            fs.readFile("./lowercaseIpsum.txt", { flag: "r", encoding: "utf-8" })]);
        })
        .then((listOfFileContents) => {
            console.log("Successfully read the contents of uppercase.txt and lowercase.txt");

            const sortedContents = listOfFileContents.reduce((sortedContent, content) => {
                content = content.replaceAll("\n", "")
                    .replaceAll(" ", "")
                    .split("")
                    .sort((a, b) => {
                        if (a < b) {
                            return -1;
                        } else if (b > a) {
                            return 1;
                        } else {
                            return 0;
                        }
                    })
                    .join("");
                
                sortedContent += content;

                return sortedContent;
            }, "");

            return fs.writeFile("./sortedLipsum.txt", sortedContents, { encoding: "utf-8", flag: "w"});
        })
        .then(() => {
            console.log("Successfully written sorted content to filenames.txt");

            return fs.writeFile("./filenames.txt", "\nsortedLipsum.txt", { encoding: "utf-8", flag: "a" });
        })
        .then(() => {
            console.log("Successfully written sortedLipsum.txt to filenames.txt");

            return fs.readFile("./filenames.txt", { flag: "r", encoding: "utf-8" });
        })
        .then((filePaths) => {
            const filesToDelete = filePaths.split("\n").map((filePath) => {
                filePath = path.join("./", filePath);

                return fs.unlink(filePath);
            });

            return Promise.all(filesToDelete);
        })
        .then(() => {
            console.log("Successfully deleted all the specified files");
        })
        .catch((err) => {
            console.log(err);
        });
}

module.exports = readAndManipulateFiles;