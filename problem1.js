/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs').promises;
const path = require("path");

function createDirectoryAndRandomJSONFiles() {
    const directoryPath = "./filesJSON";

    fs.mkdir(directoryPath)
        .then(() => {
            console.log("Directory successfully created");

            const numberOfFiles = 5;
            const iterateArray = new Array(numberOfFiles).fill(0);
            
            const filesToCreate = iterateArray.map(() => {
                const randomNumber = Math.floor(Math.random() * 100);
                const filePath = path.join(directoryPath, `${randomNumber}.json`);

                return fs.readFile(filePath, {flag: "a+", encoding: "utf-8"});
            });

            return Promise.all(filesToCreate);
        })
        .then((listOfFileContents) => {
            console.log("All files created");

            return fs.readdir("./filesJSON");
        })
        .then((filePaths) => {
            console.log("Successfully read the contents of the directory");

            const filesToDelete = filePaths.map((file) => {
                return fs.unlink(path.join("./filesJSON", file));
            });

            return Promise.all(filesToDelete);
        })
        .then(() => {
            console.log("All files have been successfully deleted");
        })
        .catch((err) => {
            console.log(err);
        });
}

module.exports = createDirectoryAndRandomJSONFiles;